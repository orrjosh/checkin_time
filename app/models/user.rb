class User < ApplicationRecord
  belongs_to :role
  belongs_to :codename, optional: true
  validates :f_name, presence: true
  validates :l_name, presence: true
  validates :username, uniqueness: true
end
