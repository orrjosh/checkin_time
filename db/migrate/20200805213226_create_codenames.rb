class CreateCodenames < ActiveRecord::Migration[6.0]
  def change
    create_table :codenames do |t|
      t.string :code_name
      t.references :user

      t.timestamps
    end
  end
end
