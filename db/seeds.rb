# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
roles = Role.create([{role_name: 'Counselor'}, {role_name: 'Student'}]) if Role.all.empty?
animals = Codename.create([{code_name: 'Cat'}, {code_name: 'Dog'}]) if Codename.all.empty?